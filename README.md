Fake virtiofsd


Exposes a filesystem with one file `the_file` that contains `ABCDE`.

In the host:
```
$ virtiofsd --socket-path=vfsd.sock &
$ qemu \
  -chardev socket,id=vfsdsock,path=vfsd.sock \
  -device vhost-user-fs-pci,queue-size=1024,chardev=vfsdsock,tag=host \
  ...

```

In the guest:
```
# mount -t virtiofs host /mnt

# ls -l /mnt/the_file
-rw-r--r-- 1 root root 6 Jan 16 14:19 /mnt/the_file

# cat /mnt/the_file
ABCDE

# umount /mnt
```

