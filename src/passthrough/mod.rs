// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

pub mod file_handle;
pub mod inode_store;
pub mod mount_fd;
pub mod stat;
pub mod util;

use crate::filesystem::{Context, Entry, FileSystem, FsOptions, OpenOptions, ZeroCopyWriter};
use crate::passthrough::inode_store::Inode;
use crate::read_dir::ReadDir;
use std::ffi::CStr;
use std::io;
use std::time::Duration;

type Handle = u64;

/// A fake file system with only one file 'the_file'
pub struct PassthroughFs;

impl PassthroughFs {
    pub fn new() -> io::Result<PassthroughFs> {
        Ok(PassthroughFs {})
    }
}

impl FileSystem for PassthroughFs {
    type Inode = Inode;
    type Handle = Handle;
    type DirIter = ReadDir<Vec<u8>>;

    fn init(&self, _capable: FsOptions) -> io::Result<FsOptions> {
        Ok(FsOptions::empty())
    }

    fn destroy(&self) {}

    fn lookup(&self, _ctx: Context, _parent: Inode, name: &CStr) -> io::Result<Entry> {
        // fake entry file for "the_file"
        let name = name.to_str().unwrap();
        if name.eq("the_file") {
            let mut st: libc::stat64 = unsafe { std::mem::zeroed() };
            st.st_dev = 41;
            st.st_ino = 24969449;
            st.st_nlink = 1;
            st.st_mode = 33188;
            st.st_size = 6;
            st.st_blksize = 4096;
            st.st_atime = 1673875182;
            st.st_atime_nsec = 216489116;
            st.st_mtime = 1673875182;
            st.st_mtime_nsec = 216489116;
            st.st_ctime = 1673875182;
            st.st_ctime_nsec = 216489116;

            Ok(Entry {
                inode: 2, // is the only file in the fs
                generation: 0,
                attr: st,
                attr_flags: 0,
                attr_timeout: Duration::from_secs(5),
                entry_timeout: Duration::from_secs(5),
            })
        } else {
            Err(io::Error::from_raw_os_error(libc::ENOENT))
        }
    }

    fn open(
        &self,
        _ctx: Context,
        _inode: Inode,
        _kill_priv: bool,
        _flags: u32,
    ) -> io::Result<(Option<Handle>, OpenOptions)> {
        // fake file handle
        Ok((Some(2_u64), OpenOptions::empty()))
    }

    fn release(
        &self,
        _ctx: Context,
        _inode: Inode,
        _flags: u32,
        _handle: Handle,
        _flush: bool,
        _flock_release: bool,
        _lock_owner: Option<u64>,
    ) -> io::Result<()> {
        Ok(())
    }

    fn read<W: io::Write + ZeroCopyWriter>(
        &self,
        _ctx: Context,
        _inode: Inode,
        _handle: Handle,
        mut w: W,
        _size: u32,
        _offset: u64,
        _lock_owner: Option<u64>,
        _flags: u32,
    ) -> io::Result<usize> {
        let file_content = vec![0x41, 0x42, 0x43, 0x44, 0x45, 0x0a];
        w.write(file_content.as_slice())
    }

    fn getattr(
        &self,
        _ctx: Context,
        _inode: Inode,
        _handle: Option<Handle>,
    ) -> io::Result<(libc::stat64, Duration)> {
        // fake stat inode 1 (shred dir)
        let mut st: libc::stat64 = unsafe { std::mem::zeroed() };
        st.st_dev = 41;
        st.st_ino = 24961754;
        st.st_nlink = 1;
        st.st_mode = 16877;
        st.st_size = 16;
        st.st_blksize = 4096;
        st.st_atime = 1673875182;
        st.st_atime_nsec = 216489116;
        st.st_mtime = 1673875182;
        st.st_mtime_nsec = 216489116;
        st.st_ctime = 1673875182;
        st.st_ctime_nsec = 216489116;

        Ok((st, Duration::from_secs(5)))
    }

    fn flush(
        &self,
        _ctx: Context,
        _inode: Inode,
        _handle: Handle,
        _lock_owner: u64,
    ) -> io::Result<()> {
        Ok(())
    }
}
